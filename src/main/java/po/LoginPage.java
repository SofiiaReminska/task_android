package po;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginPage extends BasePage {

    private static final Logger LOG = LogManager.getLogger(LoginPage.class);

    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotItButton;

    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement goToGmail;

    @AndroidFindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private MobileElement addEmail;

    @AndroidFindBy(id = "com.google.android.gm:id/account_setup_item")
    private MobileElement accountButton;

    @AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View/android.view.View[1]/" +
            "android.view.View[1]/android.widget.EditText")
    private MobileElement emailInput;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitEmailButton;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.EditText")
    private MobileElement passwordInput;

    @AndroidFindBy(xpath = "//android.widget.Button[@resource-id='signinconsentNext']")
    private MobileElement acceptTermsAndConditionsButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='MORE']")
    private MobileElement moreButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='ACCEPT']")
    private MobileElement acceptGoogleServiceButton;

    @AndroidFindBy(id = "com.google.android.gm:id/owner")
    private MobileElement owner;

    @AndroidFindBy(id = "com.google.android.gms:id/minute_maid")
    private MobileElement head;

    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitPasswordButton;


    public LoginPage(AppiumDriver driver) {
        super(driver);
    }

    public LoginPage clickGotItButton() {
        LOG.info("Clicking on got in button");
        gotItButton.click();
        return this;
    }

    public LoginPage clickAddEmail() {
        LOG.info("Clicking on add email button");
        addEmail.click();
        return this;
    }

    public LoginPage chooseAccount() {
        LOG.info("Choosing account");
        waitElementToBeVisible(accountButton);
        accountButton.click();
        return this;
    }

    public LoginPage inputEmail(String email) {
        LOG.info("Writing email");
        waitElementToBeVisible(emailInput);
        emailInput.sendKeys(email);
        return this;
    }

    public LoginPage submitEmail() {
        LOG.info("Submitting email");
        submitEmailButton.click();
        return this;
    }

    public LoginPage inputPassword(String password) {
        LOG.info("Writing password");
        waitElementToBeVisible(passwordInput);
        head.click();
        passwordInput.sendKeys(password);
        return this;
    }

    public LoginPage submitPassword() {
        LOG.info("Submitting password");
        submitPasswordButton.click();
        return this;
    }

    public LoginPage acceptTermsAndConditions() {
        LOG.info("Accepting terms and Condition");
        waitElementToBeVisible(acceptTermsAndConditionsButton);
        acceptTermsAndConditionsButton.click();
        waitElementToBeVisible(moreButton);
        moreButton.click();
        acceptGoogleServiceButton.click();
        return this;
    }

    public LoginPage goToGmail() {
        LOG.info("Going to gmail");
        waitElementToBeVisible(owner);
        goToGmail.click();
        return this;
    }
}
