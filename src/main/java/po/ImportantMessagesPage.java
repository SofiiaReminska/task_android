package po;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ImportantMessagesPage extends BasePage {

    private static final Logger LOG = LogManager.getLogger(ImportantMessagesPage.class);
    @AndroidFindBy(id = "com.google.android.gm:id/contact_image")
    private List<MobileElement> checkedMessagesList;
    @AndroidFindBy(id = "com.google.android.gm:id/delete")
    private MobileElement deleteMessagesButton;
    @FindBy(id = "com.google.android.gm:id/empty_text")
    private MobileElement noStaredMessagesText;

    public ImportantMessagesPage(AppiumDriver driver) {
        super(driver);
    }

    public ImportantMessagesPage selectImportantMessages(int count) {
        LOG.info("Selecting important messages");
        for (int i = 0; i < count; i++) {
            checkedMessagesList.get(i).click();
        }
        return this;
    }

    public ImportantMessagesPage deleteCheckedMessages() {
        LOG.info("Deleting checked messages");
        waitElementToBeVisible(deleteMessagesButton);
        deleteMessagesButton.click();
        return this;
    }

    public boolean isNoImportantMessageLabelVisible() {
        LOG.info("Checking if message label is displayed ");
        return noStaredMessagesText.isDisplayed();
    }
}
