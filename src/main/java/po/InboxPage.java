package po;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class InboxPage extends BasePage {

    private static final Logger LOG = LogManager.getLogger(ImportantMessagesPage.class);

    @AndroidFindBy(id = "com.google.android.gm:id/star")
    private List<MobileElement> importantMessageCheckBoxes;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private MobileElement mainMenuButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Starred']")
    private MobileElement importantMessagesFolder;

    public InboxPage(AppiumDriver driver) {
        super(driver);
    }

    public InboxPage markMessageAsImportant(int count) {
        LOG.info("Marking messages as important");
        importantMessageCheckBoxes.get(count).click();
        return this;
    }

    public InboxPage clickOnImportantFolder() {
        LOG.info("Clicking on important messages folder");
        mainMenuButton.click();
        waitElementToBeVisible(importantMessagesFolder);
        importantMessagesFolder.click();
        return this;
    }

    public InboxPage clickMainMenuButton() {
        LOG.info("Clicking on main menu button");
        mainMenuButton.click();
        return this;
    }
}
