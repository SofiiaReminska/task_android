package bo;

import driver.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import po.InboxPage;

public class InboxBO {
    private static final Logger LOG = LogManager.getLogger(InboxBO.class);

    private InboxPage inboxPage;

    public InboxBO() {
        inboxPage = new InboxPage(DriverManager.getDriver());
    }

    public void markMessagesAsImportant(int count) {
        for (int i = 0; i < count; i++) {
            LOG.info("Marked message {} as important", i);
            inboxPage.markMessageAsImportant(i);
        }
    }

    public void navigateToImportantFolder() {
        inboxPage.clickOnImportantFolder();
    }

    public void navigateToMainMenu() {
        inboxPage.clickMainMenuButton();
    }
}
