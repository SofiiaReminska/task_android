package bo;

import driver.DriverManager;
import po.LoginPage;

public class LoginBO {
    private LoginPage loginPage;

    public LoginBO() {
        this.loginPage = new LoginPage(DriverManager.getDriver());
    }

    public void login(String email, String password) {
        loginPage.clickGotItButton();
        loginPage.clickAddEmail();
        loginPage.chooseAccount();
        loginPage.inputEmail(email);
        loginPage.submitEmail();
        loginPage.inputPassword(password);
        loginPage.submitPassword();
        loginPage.acceptTermsAndConditions();
        loginPage.goToGmail();
    }
}
