package bo;

import driver.DriverManager;
import po.ImportantMessagesPage;

import static org.testng.Assert.assertTrue;

public class ImportantMessagesBO {

    private ImportantMessagesPage importantMessagePage = new ImportantMessagesPage(DriverManager.getDriver());

    public void areMessagesDeletedFromImportant() {
        assertTrue(importantMessagePage.isNoImportantMessageLabelVisible());
    }

    public void deleteImportantMessages(int count) {
        importantMessagePage.selectImportantMessages(count);
        importantMessagePage.deleteCheckedMessages();
    }
}
