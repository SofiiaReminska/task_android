package driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static final Logger LOG = LogManager.getLogger(DriverManager.class);

    private static ThreadLocal<AppiumDriver> driver = new ThreadLocal<>();

    private DriverManager() {

    }

    public static AppiumDriver getDriver() {
        if (driver.get() == null) {
            LOG.info("Starting browser");
            DesiredCapabilities capabilities = getDesiredCapabilities();
            try {
                driver.set(new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver.get().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        }
        return driver.get();
    }

    private static DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Pixel_3_API_29:5554");
        capabilities.setCapability(CapabilityType.VERSION, "10");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appPackage", "com.google.android.gm");
        capabilities.setCapability("appActivity", "GmailActivity");
        return capabilities;
    }

    public static void close() {
        driver.get().quit();
        driver.remove();
    }
}
