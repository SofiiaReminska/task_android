import bo.ImportantMessagesBO;
import bo.InboxBO;
import bo.LoginBO;
import driver.DriverManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class GmailTest {

    @DataProvider
    public Object[][] accounts() {
        return new Object[][]{
                new Object[]{"sofii.test@gmail.com", "QwErTyUi", 3},
                new Object[]{"sofii.test2@gmail.com", "QwErTyUi", 2},
                new Object[]{"sofiiatest3@gmail.com", "QwErTyUi", 3},
                new Object[]{"sofii.test4@gmail.com", "QwErTyUi", 4},
                new Object[]{"sofii.test5@gmail.com", "QwErTyUi", 1}
        };
    }

    @Test(dataProvider = "accounts")
    public void test(String email, String password, int numberOfMessages) {
        LoginBO loginBO = new LoginBO();
        loginBO.login(email, password);

        InboxBO inboxBO = new InboxBO();
        inboxBO.markMessagesAsImportant(numberOfMessages);
        inboxBO.navigateToMainMenu();
        inboxBO.navigateToImportantFolder();

        ImportantMessagesBO importantMessagesBO = new ImportantMessagesBO();
        importantMessagesBO.deleteImportantMessages(numberOfMessages);
        importantMessagesBO.areMessagesDeletedFromImportant();
    }

    @AfterMethod
    public void after() {
        DriverManager.close();
    }
}
